import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import User from '../views/User.vue'
import Main from '../views/Main.vue'
import Mall from '../views/Mall.vue'
import PageOne from '../views/PageOne.vue'
import PageTwo from '../views/PageTwo.vue'



Vue.use(VueRouter)
// 1 定义（路由）组件。 
// 可以从其他文件 import 进来
// 3.chuangjian router 实例，然后传 `routes` 配置

// 获取原型对象push函数
const originalPush = VueRouter.prototype.push
 
// 获取原型对象replace函数
const originalReplace = VueRouter.prototype.replace
 
// 修改原型对象中的push函数
VueRouter.prototype.push = function push(location){
return originalPush.call(this , location).catch(err=>err)
}
 
// 修改原型对象中的replace函数
VueRouter.prototype.replace = function replace(location){
return originalReplace.call(this , location).catch(err=>err)
}
const routes = [
    {
        path: '/',
        component: Main,
        redirect: '/Home',
        children: [
            { path: '/Home', component: Home },// 首页
            { path: '/User', component: User },//用户管理
            { path: '/Mall', component: Mall },//商品管理
            { path: '/Page1', component: PageOne },
            { path: '/Page2', component: PageTwo },
            
          ]
    }
    
  ]
  const router = new VueRouter({
    routes // (缩写) 相当于 routes: routes
  })
export default router
