export default{
    state:{
        isCollapse: false // 用于控制菜单的展开还是收起
    },
    mutations:{
        //修改的方法
        collapseMenu(state){
            state.isCollapse = !state.isCollapse;
        }
    }
}